from errno import EEXIST
from glob import iglob
from os import makedirs
from os.path import basename, splitext

from manipulations import jump_wavesets
from wavesets import process_file


def mkdir(parent, name):
    n = parent + splitext(basename(name))[0]
    try:
        makedirs(n)
    except OSError as e:
        if e.errno != EEXIST:
            raise
    return n + '/'


def mkname(dn, c, s, n):
    name = dn + 's' + str(s) + 'c' + str(c) + basename(n)
    return name


folder = 'samples/'
regex = folder + '*.wav'
names = list(iglob(regex))
assert(len(names) == 66)

bf = mkdir('', 'build')
f = jump_wavesets
crosses = [-0.125, -0.0625, -0.031250, 0, 0.125, 0.0625, 0.031250]
sizes = [2, 4, 8, 16, 32, 64]

for n in names:
    dn = mkdir(bf, n)
    for c in crosses:
        for s in sizes:
            outname = mkname(dn, c, s, n)
            process_file(n, outname, c, s, f)
